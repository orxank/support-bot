<?php

class PsApi
{
    private $api_key;
    private $api_url;

    public function __construct($api_key, $api_url) {
        $this->api_key = $api_key;
        $this->api_url = $api_url;
        // Check if curl is enabled.
        if (function_exists('curl_version') === false) {
            throw new Exception('Curl is not installed!');
        }
        return true;
    }
    /**
     * Get all threads
     */
    public function getThreads($options) {
        $url = $this->api_url . 'seller/threads';
        $url = $this->_handleOptions($url, $options);
        return $this->_apiCall($url, false);
    }

    /**
     * Get the info on one thread
     */
    public function getThread($id_thread) {
        $url = $this->api_url . 'seller/thread/' . (int) $id_thread . '/';
        return $this->_apiCall($url, false);
    }
    /**
     * Get all messages for one thread
     * @param int $id_thread
     */
    public function getMessages($id_thread, $options)	{
        $url = $this->api_url . 'seller/threads/' . (int) $id_thread . '/messages';
        $url = $this->_handleOptions($url, $options);

        return $this->_apiCall($url, false);
    }

    /**
     * Get all messages no matter what the thread is
     */
    public function getAllMessages($options)	{
        $url = $this->api_url . 'seller/messages';
        $url = $this->_handleOptions($url, $options);

        return $this->_apiCall($url, false);
    }
    /**
     * Send your answer to a message
     * @param string $message
     * @param file $file
     */
    public function sendMessage($id_thread, $message, $file = null)	{
        $url = $this->api_url . 'seller/threads/' . (int) $id_thread . '/messages/add';
        $post = array(
            'api_key' => $this->api_key,
            'message' => $message
        );
        if (!is_null($file) && !empty($file)) {
            $cfile = new CURLFile($_FILES['attachment']['tmp_name']);
            $post['attachment'] = $cfile;
        }
        return $this->_apiCall($url, true, $post);
    }
    /**
     * Handles the API call for basic functions
     * @param string $url
     * @param array $postFields
     */
    private function _apiCall($url, $method, $postFields = null)	{
        if (is_null($postFields)) {
            $postFields = array('api_key' => $this->api_key);
        }

        $ch = curl_init();
        if ($ch === false) {
            return 'Can not initiate curl_init';
        }
        curl_setopt($ch, CURLOPT_URL, $url);

        if ($method === false) {
            $postFields = http_build_query($postFields);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);
        } else {
            $postFields = $postFields;
            curl_setopt($ch, CURLOPT_POST, $method);
        }
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $results = curl_exec($ch);
        if ($results === false) {
            return 'curl_exec failed, error # : ' . curl_errno($ch) . ' - ' . curl_error($ch);
        }
        curl_close($ch);
        return $results;
    }
    private function pSQL($string, $htmlOK = false) {
        if (!is_numeric($string)) {
            $search = array("\\", "\0", "\n", "\r", "\x1a", "'", '"');
            $replace = array("\\\\", "\\0", "\\n", "\\r", "\Z", "\'", '\"');
            $string = str_replace($search, $replace, $string);
            if (!$htmlOK) {
                $string = strip_tags(nl2br($string));
            }
        }
        return $string;
    }

    /**
     * Handle parameters
     * @param string $url
     * @param array $options
     */
    private function _handleOptions($url, $options) {
        $build_query = array();

        if (isset($options['limit'])) {
            $build_query['limit'] = (int) $options['limit'];
        }

        if (isset($options['sort']) && (strtolower($options['sort']) == 'asc' || strtolower($options['sort']) == 'desc')
        ) {
            $build_query['sort'] = $options['sort'];
        }

        if (isset($options['page'])) {
            $build_query['page'] = (int) $options['page'];
        }

        if (count($build_query) > 0) {
            $url .= '?' . http_build_query($build_query);
        }


        return $url;
    }
}