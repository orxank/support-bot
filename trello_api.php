<?php

class TrelloApi
{
    private $url_token;
    private $api_url = 'https://api.trello.com/1/';

    /**
     * TrelloApi constructor.
     * Creates token and key side of the API request URL
     *
     * @param $api_key
     * @param $api_token
     */
    public function __construct($api_key, $api_token)
    {
        $this->url_token = '&key=' . $api_key . '&token=' . $api_token;
    }

    /**
     * check if card exists in Trello
     * @param $cardName
     *
     * @return array
     */
    private function cardExists ($cardName)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->api_url . 'search?query=' . rawurlencode($cardName) . '&cards_limit=1000&idBoards=5a0d4d93eb8bfeaa5f6e4946&card_fields=name' . $this->url_token);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = json_decode(curl_exec($ch));
        curl_close($ch);

        return $response->cards;
    }

    /**
     * Create or update card
     * @param $cardName
     * @param $description
     * @param $member
     * @param $label
     *
     * @return bool
     */
    public function createUpdateCard ($cardName, $description, $member, $label)
    {
        // all cards which names is $cardName
        $cards = $this->cardExists($cardName);
        // list ID of issue table
        $idList = '5a0d4d93eb8bfeaa5f6e4947';
        if (!empty($cards) && $this->hasOpenCard($cards)) {
            // if card exists update it
            foreach ($cards as $card) {
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $this->api_url . 'cards/' . $card->id . '?idList=' . $idList . $this->url_token);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
                $response = json_decode(curl_exec($ch));
                curl_close($ch);
            }
            return true;
        } else {
            // if card does not exist then create it
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $this->api_url . 'cards?name=' . rawurlencode('#' . $cardName) . '&desc=' . rawurlencode($description) . '&idList=' . $idList . $this->url_token);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $response = json_decode(curl_exec($ch));
            curl_close($ch);
            if (empty($response->id)) {
                return false;
            }
            return true;
        }
    }

    /**
     * Top secret
     *
     * @param $cards
     *
     * @return bool
     */
    private function hasOpenCard (&$cards) {
        foreach ($cards as $index => $card) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $this->api_url . 'cards/' . $card->id . '?' . $this->url_token);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $response = json_decode(curl_exec($ch));
            curl_close($ch);
            if ($response->closed == false) {
                return true;
            }
            unset($cards[$index]);
        }
    }
}