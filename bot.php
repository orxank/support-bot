<?php

require_once "./ps_api.php";
require_once "./trello_api.php";

class Bot
{
    private $psAPI;
    private $trelloAPI;
    private $messageCount;
    private $token;

    /**
     * Bot constructor.
     *
     * @param int $messageCount
     */
    public function __construct($messageCount)
    {
        $this->psAPI = new PsApi('5254039bb0f1cd0091ed27358ff964df', 'https://api.addons.prestashop.com/request/');
        $this->trelloAPI = new TrelloApi('21e0f30f4239f29e58b94410f44549aa', '2433603d0afbdcb58497d026f89b173ed03a0457644b76a411691a791ffeccbb');
        $this->messageCount = $messageCount;
        $this->token = 'b45m2Gx72gd9sp9';
    }

    private function checkToken($requestToken)
    {
        return $this->token == $requestToken;
    }

    /**
     * Get last messages
     *
     * @param int $mesageLimit
     * @param int $page
     *
     * @return mixed
     */
    private function getLastMessages($mesageLimit, $page = 1)
    {
        $options = array(
            'limit' => $mesageLimit,
            'sort'  => 'desc',
            'page'  => $page
        );
        return $this->psAPI->getAllMessages($options);
    }

    /**
     * Check last messages and reply them
     */
    private function checkMessagesAndReply()
    {
        // gets n recent messages
        $response = json_decode($this->getLastMessages($this->messageCount));
        // saves already messages thread IDs
        $alreadyAnsweredThreadNumbers = array();
        // check every message
        foreach ($response->messages->data as $message) {
            // if we have sent message then add it to already answered messages and skip it
            if ($message->from == 'seller') {
                $alreadyAnsweredThreadNumbers[] = $message->id_community_thread;
                continue;
            }
            // if nobody marked message as seen or message is from customer or if bot has not answered to this message yet then handle it
            if ($message->seen == '0' && !in_array($message->id_community_thread, $alreadyAnsweredThreadNumbers)) {
                // this is security belt during tests
//                if ($message->id_community_thread != 414555) {
//                    continue;
//                }
                $alreadyAnsweredThreadNumbers[] = $message->id_community_thread;
                // gets order's ID
                $orderId = json_decode($this->psAPI->getThread($message->id_community_thread))->thread->id_order;
                if ($orderId == 0) {
                    // if it is presale message then card name in Trello will be #thread's id
                    $orderId = $message->id_community_thread . ' - PreSale';
                }
                // as a card descrtiption will be the URL of the message
                $description = 'https://addons.prestashop.com/en/seller-contact.php?ict=' . $message->id_community_thread;
                // add or update card in Trello
                $this->addUpdateCardInTrello($orderId, $description);
                $options = array(
                    'limit' => 100,
                    'sort'  => 'desc',
                    'page'  => 1
                );
                // gets count of messages of the current thread
                $threadMessages = json_decode($this->psAPI->getMessages($message->id_community_thread, $options))->messages;
                $countOfThreadMessages = $threadMessages->total;
                // check if last message replied by us or previous is replied by the bot then do not write to a customer
                if ($threadMessages->data[0]->from == 'seller' || $this->botAlreadyHasReplied($threadMessages->data)) {
                    continue;
                }
                $isFirstMessage = false;
                if ($countOfThreadMessages == 1) {
                    // if count is one then it is the first message of a customer
                    $isFirstMessage = true;
                }
                // write message to a customer
                $this->writeMessage($message->id_community_thread, $isFirstMessage);
            } else {
                continue;
            }
        }
    }

    /**
     * Write message to acustomer
     *
     * @param int  $id_thread
     * @param bool $isFirstMessage
     */
    private function writeMessage($id_thread, $isFirstMessage)
    {
        $messages = require_once "./messages.php";
        if ($isFirstMessage) {
            $response = json_decode($this->psAPI->sendMessage($id_thread, $messages['std']));
            sleep(30);
        } else {
            $response = json_decode($this->psAPI->sendMessage($id_thread, $messages['std']));
            sleep(30);
        }
    }

    /**
     * Add or update a card in Trello
     *
     * @param string $orderId
     * @param string $description
     * @param bool   $member
     * @param bool   $label
     */
    private function addUpdateCardInTrello($orderId, $description, $member = false, $label = false)
    {
        $trello = $this->trelloAPI;
        $trello->createUpdateCard($orderId, $description, $member, $label);
    }

    /**
     * @param $messages
     *
     * @return bool
     */
    private function botAlreadyHasReplied($messages)
    {
        // check every message
        foreach ($messages as $message) {
            // if we replied to this thread ever
            if ($message->from == 'seller') {
                // check who replied to the previous message
                if (preg_match('/auto-reply/', $message->message)) {
                    // last message has sent by the bot
                    return true;
                }
                return false;
            }
        }
        // we have not relpied to this thread ever
        return false;
    }

    /**
     * Run the bot
     */
    public function run($requestToken)
    {
        if ($this->checkToken($requestToken)) {
            $this->checkMessagesAndReply();
        } else {
            die("Token is false!");
        }
    }
}

$requestToken = $_GET['token'];
$bot = new Bot(50);
$bot->run($requestToken);
